#!/bin/bash -ex

set -ex

cp -R /tmp/.ssh /root
chmod 700 /root/.ssh
chmod 600 /root/.ssh/*
chmod 644 /root/.ssh/*.pub
chown root ~/.ssh/config

exec "$@"