FROM php:7.4.28-fpm

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

# these are slow steps so makes sense to have sepearte RUN for each extension
# so on the failure we don't need to run the previous step agin.
# https://github.com/mlocati/docker-php-extension-installer
RUN install-php-extensions bcmath
RUN install-php-extensions exif
RUN install-php-extensions gd
RUN install-php-extensions intl
RUN install-php-extensions pdo_mysql
RUN install-php-extensions tidy
RUN install-php-extensions xdebug
RUN install-php-extensions sodium

RUN apt update && \
    apt upgrade -y && \
    apt install -y g++ autoconf git ssh make msmtp zip unzip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -y nodejs
RUN npm install --global yarn

COPY conf.d/php.ini /usr/local/etc/php/conf.d

WORKDIR  /srv
EXPOSE 6001


COPY entry.bash /
RUN chmod +x /entry.bash

ENTRYPOINT ["/entry.bash"]
CMD ["php-fpm"]
