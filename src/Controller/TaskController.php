<?php

namespace App\Controller;

use App\Entity\Task;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{

    /**
     * @Route("/crud", methods={"GET"})
     */
    public function crud()
    {
        return $this->render('index.html.twig');
    }
    /**
     * @Route("/api/tasks", methods={"GET"})
     */
    public function index()
    {
//        return $this->render('index.html.twig');
        $repository = $this->getDoctrine()->getRepository(Task::class);
        return new JsonResponse($repository->findAll());
    }

    /**
     * @Route("/api/tasks", methods={"POST"})
     */
    public function store(Request $request)
    {
        $body = json_decode($request->getContent(), true);
        if (isset($body['title'])){
            $title = $body['title'];
            $entityManager = $this->getDoctrine()->getManager();
            $task = new Task();
            $task->setTitle($title);
            $entityManager->persist($task);
            $entityManager->flush();
            return new JsonResponse($task);
        }
        return new Response('please enter a valid title in json format', '400');
    }

    /**
     * @Route("/api/tasks/{id}", methods={"GET"})
     */
    public function show(int $id): Response
    {
        $task = $this->getDoctrine()
            ->getRepository(Task::class)
            ->find($id);

        if (!$task) {
            return new JsonResponse('No task found for id '.$id, '404');
        }

        return new JsonResponse($task);
    }

    /**
     * @Route("/api/tasks/{id}", methods={"PUT"})
     */
    public function update($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $task = $entityManager->getRepository(Task::class)->find($id);
        if ($task) {
            $body = json_decode($request->getContent(), true);
            if (isset($body['title'])) {
                $title = $body['title'];
                $task->setTitle($title);
                $entityManager->flush();
                return new JsonResponse($task);
            }
            return new Response('please enter a valid title in json format', '400');
        }
        return new JsonResponse('No task found for id '.$id, '404');
    }

    /**
     * @Route("/api/tasks/{id}", methods={"DELETE"})
     */
    public function destroy($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $task = $entityManager->getRepository(Task::class)->find($id);

        if (!$task) {
            return new JsonResponse('No task found for id '.$id, '400');
        }

        $entityManager->remove($task);
        $entityManager->flush();

        return new JsonResponse('deleted successfully');
    }

}