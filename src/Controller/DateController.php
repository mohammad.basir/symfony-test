<?php

namespace App\Controller;

use App\Entity\Task;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DateController extends AbstractController
{

    /**
     * @Route("/time", methods={"GET"})
     */
    public function time()
    {
        return $this->render('time.html.twig');
    }

}