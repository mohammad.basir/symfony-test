<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Salarmehr\Cosmopolitan\Cosmo;


class AppExtension extends AbstractExtension
{

    public function getFilters()
    {
        return [
            new TwigFilter('convertTo', [$this, 'convertTo']),
        ];
    }

    public function convertTo($date, string $type)
    {
        switch ($type) {
            case 'jalali':
                $cosmo = new Cosmo('fa_IR', ['timezone' => 'Asia/Tehran']);
                return $cosmo->date($date, 'full');
            case 'ghamari':
                $cosmo = new Cosmo('ar_EG', ['timezone' => 'Africa/Cairo']);
                return $cosmo->date($date, 'full');
            case 'gregorain':
                $cosmo = new Cosmo('en_AU', ['timezone' => 'Australia/Sydney']);
                return $cosmo->date($date, 'full');
        }
    }

}